### Building A Release
```
> vsce publish
```

### Building A Local Copy
```
> vsce package --baseImagesUrl https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/raw/master/images/
```