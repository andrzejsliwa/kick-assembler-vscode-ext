# Roadmap

This extension is a work in progress, there is much left to be done.

### In Progress
* hover support for common memory locations like $d020
* auto-completion support

### Backburner
* auto-completion support from all imports
* refactoring support
* breakpoints and watches

### Completed
* ~~basic support for emulators (like vice)~~
* ~~basic support for C64Debugger~~
* ~~syntax highlighting~~
* ~~basic error checking~~
* ~~hover support for definitions~~
* ~~hover support for opcodes~~
* ~~hover support for macros and pseudopcodes~~
* ~~hover support for literals~~
* ~~hover support for directives~~
* ~~basic outline support~~